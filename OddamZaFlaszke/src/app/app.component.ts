import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MessageModel } from './models/message-model';
import { AppLink } from './app-link';
import { AppLinkAuthorizationMode } from './enums/app-link-authorization-mode.enum';

import { AuthenticationService } from './services/authentication.service';
import { MessageService } from './services/message.service';
import { LogService } from './services/log.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  messages: MessageModel[] = [];
  current_date: Date;
  links: AppLink[] = [
    new AppLink('Home', '/', AppLinkAuthorizationMode.Any),
    new AppLink('Register', '/register', AppLinkAuthorizationMode.NotAuthenticated),
    new AppLink('Login', '/login', AppLinkAuthorizationMode.NotAuthenticated),
    new AppLink('Auctions', '/auctions', AppLinkAuthorizationMode.Authenticated),
    new AppLink('My auctions', '/myauctions', AppLinkAuthorizationMode.Authenticated)
  ];

  constructor(public authenticationService: AuthenticationService,
    private messageService: MessageService,
    private logService: LogService,
    private router: Router) {
    this.messageService.subscribeSuccess(message => this.pushMessage(message, true));

    this.messageService.subscribeError(message => this.pushMessage(message, false));
  }

  ngOnInit() {
    setInterval(() => {
      this.updateCurrentDate();
    }, 1000);
  }

  private logout(): void {
    this.authenticationService.logout()
      .then(() => {
        this.messageService.success('Logout success');

        this.router.navigate(['']);
      })
      .catch(reason => {
        this.logService.error('logout failed', reason);

        this.messageService.error('Logout error');
      });
  }

  private updateCurrentDate(): void {
    this.current_date = new Date();
  }

  private pushMessage(message: string, success: boolean): void {
    const expires_date = new Date();

    expires_date.setSeconds(expires_date.getSeconds() + 5);

    this.messages.push({
      Message: message,
      ExpiresDate: expires_date,
      Success: success
    });
  }
}
