export enum AppLinkAuthorizationMode {
    Any,
    Authenticated,
    NotAuthenticated
}
