import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuctionCreateModel } from '../models/auction-create-model';
import { AuctionService } from '../services/auction.service';
import { MessageService } from '../services/message.service';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-auction-create',
  templateUrl: './auction-create.component.html',
  styleUrls: ['./auction-create.component.css']
})
export class AuctionCreateComponent implements OnInit {
  model: AuctionCreateModel;
  form: FormGroup;

  constructor(private auctionService: AuctionService,
    private messageService: MessageService,
    private logService: LogService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.model = { Description: '' };
  }

  onSubmit() {
    this.logService.debug('create auction', this.model);
    this.auctionService.createAuction(this.model)
      .then(auction => {
        this.messageService.success('Auction successfully created');

        this.router.navigate(['auction', auction.Id]);
      })
      .catch(reason => {
        this.logService.error('submit failed', reason);

        this.messageService.error('Auction creation failed');
      });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Description': [this.model.Description, [Validators.required]],
      'EndDate': [this.model.EndDate, [Validators.required]]
    });
  }
}
