import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuctionDescriptionModel } from '../models/auction-description-model';

import { AuctionService } from '../services/auction.service';
import { LogService } from '../services/log.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.component.html',
  styleUrls: ['./auction.component.css']
})
export class AuctionComponent implements OnInit {
  @Input() auction: AuctionDescriptionModel;

  constructor(private router: Router,
    private auctionService: AuctionService,
    private logService: LogService,
    private messageService: MessageService) { }

  ngOnInit() { }

  goToDetail() { this.router.navigate(['auction', this.auction.Id]); }
}
