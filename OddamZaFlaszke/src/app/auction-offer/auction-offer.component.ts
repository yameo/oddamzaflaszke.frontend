import 'rxjs/add/operator/switchMap';

import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { OfferCreateModel } from '../models/offer-create-model';
import { AuctionService } from '../services/auction.service';
import { MessageService } from '../services/message.service';
import { LogService } from '../services/log.service';
import { CurrencyType } from '../enums/currency-type.enum';
import { CurrencyItemModel } from '../models/currency-item-model';

import { MoreThanZeroValidator } from './more-than-zero';

@Component({
  selector: 'app-auction-offer',
  templateUrl: './auction-offer.component.html',
  styleUrls: ['./auction-offer.component.css']
})
export class AuctionOfferComponent implements OnInit {
  private auctionId: number;
  model: OfferCreateModel;
  currencies: CurrencyItemModel[];
  form: FormGroup;

  constructor(private auctionService: AuctionService,
    private messageService: MessageService,
    private logService: LogService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.model = { Comment: '' };
    this.currencies = [
      { Name: 'Beer', Value: CurrencyType.Beer },
      { Name: 'Vodka', Value: CurrencyType.Vodka },
      { Name: 'Wine', Value: CurrencyType.Wine }
    ];
  }

  onSubmit() {
    this.logService.debug('create offer', this.auctionId, this.model);
    this.auctionService.offerAuction(this.auctionId, this.model)
      .then(offer => {
        this.messageService.success('Offer successfully created');

        this.router.navigate(['auction', offer.AuctionId]);
      })
      .catch(reason => {
        this.logService.error('submit failed', reason);

        this.messageService.error('Offer creation failed');
      });
  }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => params['id'])
      .subscribe(id => this.auctionId = (id as number));

    this.form = this.formBuilder.group({
      'Comment': [''],
      'Quantity': [null, [Validators.required, MoreThanZeroValidator]],
      'CurrencyType': [null, [Validators.required]]
    });
  }
}
