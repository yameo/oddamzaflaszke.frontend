import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function MoreThanZeroValidator(control: AbstractControl): ValidationErrors | null {
    const value = control.value;

    return value > 0 ? null : { 'moreThanZero': { value } };
}
