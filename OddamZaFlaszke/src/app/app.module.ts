import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs';

import { AuctionService } from './services/auction.service';
import { UserService } from './services/user.service';
import { StorageService } from './services/storage.service';
import { ApiService } from './services/api.service';
import { AuthenticationService } from './services/authentication.service';
import { MessageService } from './services/message.service';
import { LogService } from './services/log.service';

import { AuthGuard } from './auth.guard';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuctionsComponent } from './auctions/auctions.component';
import { AuctionComponent } from './auction/auction.component';
import { AuctionDetailsComponent } from './auction-details/auction-details.component';
import { MyAuctionsComponent } from './my-auctions/my-auctions.component';
import { AuctionCreateComponent } from './auction-create/auction-create.component';
import { AuctionOfferComponent } from './auction-offer/auction-offer.component';
import { TitleComponent } from './title/title.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import { AuctionEditComponent } from './auction-edit/auction-edit.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'auctions', canActivate: [AuthGuard], component: AuctionsComponent },
  { path: 'auction/:id', canActivate: [AuthGuard], component: AuctionDetailsComponent },
  { path: 'myauctions', canActivate: [AuthGuard], component: MyAuctionsComponent },
  { path: 'auctioncreate', canActivate: [AuthGuard], component: AuctionCreateComponent },
  { path: 'auctionedit/:id', canActivate: [AuthGuard], component: AuctionEditComponent },
  { path: 'auctionoffer/:id', canActivate: [AuthGuard], component: AuctionOfferComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AuctionsComponent,
    AuctionComponent,
    AuctionDetailsComponent,
    MyAuctionsComponent,
    AuctionCreateComponent,
    AuctionOfferComponent,
    TitleComponent,
    OfferListComponent,
    AuctionEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [
    UserService,
    StorageService,
    ApiService,
    AuthenticationService,
    MessageService,
    LogService,
    AuctionService,
    AuthGuard
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
