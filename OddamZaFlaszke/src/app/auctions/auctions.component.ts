import { Component, OnInit } from '@angular/core';

import { AuctionDescriptionModel } from '../models/auction-description-model';

import { AuctionService } from '../services/auction.service';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit {
  auctions: AuctionDescriptionModel[] = [];

  constructor(private auctionService: AuctionService, private logService: LogService) { }

  ngOnInit() {
    this.logService.debug('get auctions');

    this.auctionService.getPublicAuctions()
      .then(auctions => this.auctions = auctions)
      .catch(reason => this.logService.error(reason));
  }
}
