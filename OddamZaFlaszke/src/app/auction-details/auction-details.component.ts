import 'rxjs/add/operator/switchMap';

import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';

import { AuctionDetailsModel } from '../models/auction-details-model';

import { LogService } from '../services/log.service';
import { AuctionService } from '../services/auction.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-auction-details',
  templateUrl: './auction-details.component.html',
  styleUrls: ['./auction-details.component.css']
})
export class AuctionDetailsComponent implements OnInit {
  auction: AuctionDetailsModel;

  constructor(private route: ActivatedRoute,
    private logService: LogService,
    private auctionService: AuctionService,
    private router: Router,
    private messageService: MessageService) { }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => params['id'])
      .subscribe(id => this.loadAuction(id as number));
  }

  loadAuction(id: number) {
    this.logService.debug('load auction details', id);

    this.auctionService.getAuction(id)
      .then(auction => {
        this.auction = auction;

        this.logService.debug('auction', this.auction);
      })
      .catch(reason => this.logService.error(reason));
  }

  goToOffer() { this.router.navigate(['auctionoffer', this.auction.Auction.Id]); }

  editAuction() { this.router.navigate(['auctionedit', this.auction.Auction.Id]); }

  closeAuction() {
    this.logService.debug('close auction');

    this.auctionService.closeAuction(this.auction.Auction.Id, { SelectedOfferId: undefined })
      .then(model => {
        this.messageService.success('Auction successfully closed');

        this.router.navigate(['auction', this.auction.Auction.Id]);
      })
      .catch(reason => {
        this.logService.error('submit failed', reason);

        this.messageService.error('Auction closing failed');
      });
  }
}
