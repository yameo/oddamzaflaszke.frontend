import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';

import { UserRegisterModel } from '../models/user-register-model';
import { UserLoginModel } from '../models/user-login-model';

import { ApiService } from './api.service';
import { AuthenticationService } from './authentication.service';
import { LogService } from './log.service';

@Injectable()
export class UserService {
  constructor(private apiService: ApiService, private authenticationService: AuthenticationService, private logService: LogService) { }

  public register(model: UserRegisterModel): Promise<void> {
    this.logService.debug('register user', model);

    return new Promise<void>((resolve, reject) => {
      this.apiService.request('accounts', RequestMethod.Post, model, 'application/json')
        .then(() => resolve())
        .catch(reject);
    });
  }

  public authenticate(model: UserLoginModel): Promise<void> {
    this.logService.debug('authenticate user', model);

    return this.authenticationService.authenticate(model);
  }
}
