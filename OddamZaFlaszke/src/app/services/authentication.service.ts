import { Injectable, EventEmitter } from '@angular/core';
import { RequestMethod } from '@angular/http';

import { UserLoginModel } from '../models/user-login-model';
import { UserAuthDataModel } from '../models/user-auth-data-model';

import { StorageService } from './storage.service';
import { ApiService } from './api.service';

@Injectable()
export class AuthenticationService {
  private authenticated = false;

  constructor(private storageService: StorageService, private apiService: ApiService) {
    this.checkAuthenticationStatus();
  }

  public isAuthenticated(): boolean {
    return this.authenticated;
  }

  private checkAuthenticationStatus(): void {
    this.getToken().then(() => { }).catch(() => { });
  }

  public authenticate(model: UserLoginModel): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const body = `username=${encodeURI(model.Username)}&password=${encodeURI(model.Password)}&grant_type=password`;

      this.apiService.request('oauth/token', RequestMethod.Post, body, 'application/x-www-form-urlencoded')
        .then(response => {
          const authData = response.json();

          const current_date = new Date();

          const expires_date = new Date(current_date);

          if (authData.expires_in !== undefined) {
            expires_date.setSeconds(expires_date.getSeconds() + authData.expires_in);
          }

          const authDataModel: UserAuthDataModel = {
            AccessToken: authData.access_token,
            Username: authData.userName,
            ExpiresDate: expires_date
          };

          this.storageService.saveSessionValue('auth_data', authDataModel);

          this.checkAuthenticationStatus();

          resolve();
        })
        .catch(reason => {
          this.checkAuthenticationStatus();

          reject(reason);
        });
    });
  }

  public logout(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.storageService.removeSessionValue('auth_data');

      this.checkAuthenticationStatus();

      resolve();
    });
  }

  public getToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const authData: UserAuthDataModel | undefined = this.storageService.getSessionValue('auth_data');
      if (authData) {
        const expires_date: Date = new Date(authData.ExpiresDate);

        if (expires_date > new Date()) {
          this.authenticated = true;

          resolve(authData.AccessToken);
        } else {
          this.authenticated = false;

          reject('Token expired');
        }
      } else {
        this.authenticated = false;

        reject('There is no valid token in session storage');
      }
    });
  }

  public getUserName(): string {
    const authData: UserAuthDataModel | undefined = this.storageService.getSessionValue('auth_data');
    if (authData) {
      return authData.Username;
    }

    return '';
  }
}
