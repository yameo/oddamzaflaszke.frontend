import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor() { }

  private getKey(key: string): string {
    return `hackaton_${key}`;
  }

  private removeStorageValue(storage: Storage, key: string): void {
    storage.removeItem(this.getKey(key));
  }

  private saveStorageValue(storage: Storage, key: string, data: any): void {
    storage.setItem(this.getKey(key), JSON.stringify(data));
  }

  private getStorageValue(storage: Storage, key: string): any {
    const item = storage.getItem(this.getKey(key));

    if (item === undefined) {
      return undefined;
    }

    return JSON.parse(item);
  }

  public removeSessionValue(key: string): void {
    this.removeStorageValue(window.sessionStorage, key);
  }

  public saveSessionValue(key: string, data: any): void {
    this.saveStorageValue(window.sessionStorage, key, data);
  }

  public getSessionValue(key: string): any {
    return this.getStorageValue(window.sessionStorage, key);
  }

  public removeValue(key: string): void {
    this.removeStorageValue(window.localStorage, key);
  }

  public saveValue(key: string, data: any): void {
    this.saveStorageValue(window.localStorage, key, data);
  }

  public getValue(key: string): any {
    return this.getStorageValue(window.localStorage, key);
  }
}
