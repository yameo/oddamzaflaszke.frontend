import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  constructor() { }

  trace(message: string, ...optionalParams: any[]): void {
    console.log('trace', message, ...optionalParams);
  }

  debug(message: string, ...optionalParams: any[]): void {
    console.log('debug', message, ...optionalParams);
  }

  info(message: string, ...optionalParams: any[]): void {
    console.log('info', message, ...optionalParams);
  }

  warn(message: string, ...optionalParams: any[]): void {
    if (typeof console.warn !== 'undefined') {
      console.warn(message, ...optionalParams);
    } else {
      console.log('warn', message, ...optionalParams);
    }
  }

  error(message: string, ...optionalParams: any[]): void {
    if (typeof console.error !== 'undefined') {
      console.error(message, ...optionalParams);
    } else {
      console.log('error', message, ...optionalParams);
    }
  }

  fatal(message: string, ...optionalParams: any[]): void {
    if (typeof console.error !== 'undefined') {
      console.error('fatal error', message, ...optionalParams);
    } else {
      console.log('fatal', message, ...optionalParams);
    }
  }
}