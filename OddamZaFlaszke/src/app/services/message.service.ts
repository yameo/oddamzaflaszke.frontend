import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class MessageService {
  private errorEmitter = new EventEmitter<string>();
  private successEmitter = new EventEmitter<string>();

  constructor() { }

  public success(message: string): void {
    this.successEmitter.emit(message);
  }

  public error(message: string): void {
    this.errorEmitter.emit(message);
  }

  public subscribeSuccess(callback: (message: string) => void): void {
    this.successEmitter.subscribe(callback);
  }

  public subscribeError(callback: (message: string) => void): void {
    this.errorEmitter.subscribe(callback);
  }
}
