import { Injectable } from '@angular/core';
import { RequestMethod } from '@angular/http';

import { ApiService } from './api.service';
import { AuthenticationService } from './authentication.service';
import { LogService } from './log.service';

import { AuctionDescriptionModel } from '../models/auction-description-model';
import { AuctionDetailsModel } from '../models/auction-details-model';
import { AuctionCreateModel } from '../models/auction-create-model';
import { AuctionClosingModel } from '../models/auction-closing-model';
import { OfferCreateModel } from '../models/offer-create-model';
import { OfferDescriptionModel } from '../models/offer-description-model';

@Injectable()
export class AuctionService {
  constructor(private apiService: ApiService, private authenticationService: AuthenticationService, private logService: LogService) { }

  private getAuctions(resource: string): Promise<AuctionDescriptionModel[]> {
    this.logService.debug('auction service getAuctions', resource);

    return new Promise<AuctionDescriptionModel[]>((resolve, reject) => {
      this.authenticationService.getToken()
        .then(token => this.apiService.request(resource, RequestMethod.Get, undefined, 'application/json', token))
        .then(response => resolve(response.json()))
        .catch(reject);
    });
  }

  public getPublicAuctions(): Promise<AuctionDescriptionModel[]> {
    return this.getAuctions('auctions/public');
  }

  public getMyAuctions(): Promise<AuctionDescriptionModel[]> {
    return this.getAuctions('auctions/my');
  }

  public getAuction(id: number): Promise<AuctionDetailsModel> {
    this.logService.debug('auction service getAuction', id);

    return new Promise<AuctionDetailsModel>((resolve, reject) => {
      this.authenticationService.getToken()
        .then(token => this.apiService.request(`auctions/${id}`, RequestMethod.Get, undefined, 'application/json', token))
        .then(response => resolve(response.json()))
        .catch(reject);
    });
  }

  public createAuction(model: AuctionCreateModel): Promise<AuctionDescriptionModel> {
    this.logService.debug('auction service createAuction', model);

    return new Promise<AuctionDescriptionModel>((resolve, reject) => {
      this.authenticationService.getToken()
        .then(token => this.apiService.request('auctions', RequestMethod.Post, model, 'application/json', token))
        .then(response => resolve(response.json()))
        .catch(reject);
    });
  }

  public updateAuction(model: AuctionCreateModel): Promise<AuctionDescriptionModel> {
    this.logService.debug('auction service updateAuction', model);

    return new Promise<AuctionDescriptionModel>((resolve, reject) => {
      this.authenticationService.getToken()
        .then(token => this.apiService.request('auctions', RequestMethod.Put, model, 'application/json', token))
        .then(response => resolve(response.json()))
        .catch(reject);
    });
  }

  public offerAuction(auctionId: number, model: OfferCreateModel): Promise<OfferDescriptionModel> {
    this.logService.debug('auction service offerAuction', auctionId, model);

    return new Promise<OfferDescriptionModel>((resolve, reject) => {
      this.authenticationService.getToken()
        .then(token => this.apiService.request(`auctions/${auctionId}/offers`, RequestMethod.Post, model, 'application/json', token))
        .then(response => resolve(response.json()))
        .catch(reject);
    });
  }

  public closeAuction(auctionId: number, model: AuctionClosingModel): Promise<AuctionClosingModel> {
    this.logService.debug('auction service closeAuction', auctionId, model);

    return new Promise<OfferDescriptionModel>((resolve, reject) => {
      reject('API request not implemented');

      // TODO: API request
    });
  }
}
