import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptionsArgs, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { LogService } from './log.service';

@Injectable()
export class ApiService {
  private static baseUrl = 'http://localhost/OddamZaFlaszke.Api'; // move to some config

  constructor(private http: Http, private logService: LogService) { }

  private getApiUrl(path: string): string {
    return `${ApiService.baseUrl}/${path}`;
  }

  public request(resource: string, method: RequestMethod, body: any, contentType: string, token?: string): Promise<Response> {
    return new Promise<Response>((resolve, reject) => {
      const url: string = this.getApiUrl(resource);

      this.logService.debug('send request to endpoint', url);

      const options: RequestOptionsArgs = {
        headers: new Headers({ 'Content-Type': contentType }),
        body: body,
        method: method
      };

      if (token !== undefined) {
        options.headers.append('Authorization', `Bearer ${token}`);
      }

      this.http.request(url, options)
        .toPromise()
        .then(response => {
          if (response.ok) {
            resolve(response);
          } else {
            reject(response.statusText);
          }
        })
        .catch(reject);
    });
  }
}
