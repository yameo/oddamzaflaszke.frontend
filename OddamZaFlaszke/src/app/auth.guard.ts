import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { LogService } from './services/log.service';
import { AuthenticationService } from './services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private logService: LogService,
    private authenticationService: AuthenticationService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.logService.debug('AuthGuard#canActivate called', state.url);

    if (this.authenticationService.isAuthenticated()) {
      this.logService.debug('is authenticated, redirect to url', state.url);

      return true;
    }

    this.logService.debug('not authenticated, redirect to login');

    this.router.navigate(['/login']);

    return false;
  }
}
