export interface UserAuthDataModel {
    Username: string;
    AccessToken: string;
    ExpiresDate: Date;
}
