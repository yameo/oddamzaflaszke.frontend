import { AuctionDescriptionModel } from './auction-description-model';
import { OfferDescriptionModel } from './offer-description-model';

export interface AuctionDetailsModel {
    Auction: AuctionDescriptionModel;
    Offers: OfferDescriptionModel[];
}
