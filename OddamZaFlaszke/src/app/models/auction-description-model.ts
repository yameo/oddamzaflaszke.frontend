export interface AuctionDescriptionModel {
    Id: number;
    Owner: string;
    Description: string;
    CreationDate: Date;
    EndDate: Date;
    CloseDate?: Date;
}
