import { CurrencyType } from '../enums/currency-type.enum';

export interface OfferDescriptionModel {
    Id: number;
    AuctionId: number;
    Owner: string;
    Quantity: number;
    Selected: boolean;
    CurrencyType: CurrencyType;
    CreationDate: Date;
    Comment: string;
}
