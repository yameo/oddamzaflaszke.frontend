export interface AuctionCreateModel {
    Description: string;
    EndDate?: Date;
}
