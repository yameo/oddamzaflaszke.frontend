export interface MessageModel {
    Message: string;
    ExpiresDate: Date;
    Success: boolean;
}
