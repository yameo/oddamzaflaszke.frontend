import { CurrencyType } from '../enums/currency-type.enum';

export interface CurrencyItemModel {
    Value: CurrencyType;
    Name: string;
}
