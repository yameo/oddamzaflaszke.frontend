import { AuctionClosingModel } from './auction-closing-model';

export interface AuctionClosedModel extends AuctionClosingModel {
    SelectedOfferId?: number;
    CloseDate?: Date;
}
