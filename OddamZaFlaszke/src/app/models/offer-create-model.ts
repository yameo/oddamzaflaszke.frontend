import { CurrencyType } from '../enums/currency-type.enum';

export interface OfferCreateModel {
    Quantity?: number;
    CurrencyType?: CurrencyType;
    Comment: string;
}
