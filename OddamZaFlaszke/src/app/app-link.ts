import { AppLinkAuthorizationMode } from './enums/app-link-authorization-mode.enum';

export class AppLink {
    constructor(
        public Name: string,
        public Route: string,
        public Mode: AppLinkAuthorizationMode
    ) { }

    public visible(authenticated: boolean): boolean {
        switch (this.Mode) {
            case AppLinkAuthorizationMode.Authenticated:
                return authenticated;
            case AppLinkAuthorizationMode.NotAuthenticated:
                return !authenticated;
        }

        return true;
    }
}
