import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { UserLoginModel } from '../models/user-login-model';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: UserLoginModel;
  form: FormGroup;

  constructor(private userService: UserService,
    private messageService: MessageService,
    private logService: LogService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.model = { Username: '', Password: '' };
  }

  onSubmit() {
    this.userService.authenticate(this.model)
      .then(() => {
        this.messageService.success('Login success');

        this.router.navigate(['']);
      })
      .catch(reason => {
        this.logService.error('submit failed', reason);

        this.messageService.error('Login error');
      });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Username': [this.model.Username, [Validators.required, Validators.maxLength(20)]],
      'Password': [this.model.Password, [Validators.required]]
    });
  }
}
