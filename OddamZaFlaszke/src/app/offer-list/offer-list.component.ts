import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuctionDetailsModel } from '../models/auction-details-model';
import { OfferDescriptionModel } from '../models/offer-description-model';

import { LogService } from '../services/log.service';
import { AuctionService } from '../services/auction.service';
import { MessageService } from '../services/message.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.css']
})
export class OfferListComponent implements OnInit {
  @Input() auction: AuctionDetailsModel;

  constructor(private logService: LogService,
    private auctionService: AuctionService,
    private router: Router,
    private messageService: MessageService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() { }

  isOwnerAuction() {
    return this.auction.Auction.Owner === this.authenticationService.getUserName();
  }

  selectOffer(offer: OfferDescriptionModel) {
    this.logService.debug('select offer');

    this.auctionService.closeAuction(this.auction.Auction.Id, { SelectedOfferId: offer.Id })
      .then(model => {
        this.messageService.success('Auction successfully closed');

        this.router.navigate(['auction', this.auction.Auction.Id]);
      })
      .catch(reason => {
        this.logService.error('submit failed', reason);

        this.messageService.error('Auction closing failed');
      });
  }
}
