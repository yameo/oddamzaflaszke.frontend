import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { UserRegisterModel } from '../models/user-register-model';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: UserRegisterModel;
  form: FormGroup;

  constructor(private userService: UserService,
    private messageService: MessageService,
    private logService: LogService,
    private formBuilder: FormBuilder) {
    this.model = { Email: '', Username: '', Password: '' };
  }

  onSubmit() {
    this.userService.register(this.model)
      .then(() => {
        this.messageService.success('User successfully registered');
      })
      .catch(reason => {
        this.logService.error('submit failed', reason);

        this.messageService.error('User registration failed');
      });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      'Username': [this.model.Username, [Validators.required, Validators.maxLength(20)]],
      'Email': [this.model.Email, [Validators.required]],
      'Password': [this.model.Password, [Validators.required]]
    });
  }
}
