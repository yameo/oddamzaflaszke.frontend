import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuctionDescriptionModel } from '../models/auction-description-model';

import { AuctionService } from '../services/auction.service';
import { LogService } from '../services/log.service';

@Component({
  selector: 'app-my-auctions',
  templateUrl: './my-auctions.component.html',
  styleUrls: ['./my-auctions.component.css']
})
export class MyAuctionsComponent implements OnInit {
  auctions: AuctionDescriptionModel[] = [];

  constructor(private auctionService: AuctionService, private logService: LogService, private router: Router) { }

  ngOnInit() {
    this.logService.debug('get auctions');

    this.auctionService.getMyAuctions()
      .then(auctions => this.auctions = auctions)
      .catch(reason => this.logService.error(reason));
  }

  goToCreate() { this.router.navigate(['auctioncreate']); }
}
