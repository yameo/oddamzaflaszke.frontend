import 'rxjs/add/operator/switchMap';

import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';

import { LogService } from '../services/log.service';

@Component({
  selector: 'app-auction-edit',
  templateUrl: './auction-edit.component.html',
  styleUrls: ['./auction-edit.component.css']
})
export class AuctionEditComponent implements OnInit {
  private auctionId: number;

  constructor(private route: ActivatedRoute,
    private logService: LogService,
    private router: Router) { }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => params['id'])
      .subscribe(id => this.loadAuction(id as number));
  }

  loadAuction(id: number) {
    this.logService.debug('load auction details', id);

    this.auctionId = id;
  }
}
