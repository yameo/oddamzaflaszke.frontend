import { OddamZaFlaszkePage } from './app.po';

describe('oddam-za-flaszke App', () => {
  let page: OddamZaFlaszkePage;

  beforeEach(() => {
    page = new OddamZaFlaszkePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
